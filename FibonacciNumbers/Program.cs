﻿using System;
/* Task 7
 * Each new term in the Fibonacci sequence is generated by adding the previous two terms.
 * By starting with 1 and 2, the first 10 terms will be: 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
 * By considering the terms in the Fibonacci sequence whose values do not exceed four million,
 * Write a console app to find the sum of the even-valued terms.
 */
namespace FibonacciNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            // Num1 is the fisrt number of Fibonacci
            // Num2 is the second number of Fibonacci
            // Num3 is the third number of Fibonacci
            // Sum is the sumation of the even numbers.


            int Num1 = 0, num2 = 1, num3 = 0, sum = 0;

            while (num3 < 4000000)
            {
                // Check the even number.
                if (num3 % 2 == 0)
                {
                    sum += num3;
                }
                num3 = Num1 + num2;
                // Shift one number to the right by swaping.
                Num1 = num2;
                num2 = num3;
            }
            Console.WriteLine($"The summation of the even numbers is:{sum}");

        }
    }
}
